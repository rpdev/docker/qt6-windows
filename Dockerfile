FROM fedora:rawhide

ARG WINARCH=mingw64
ARG WINARCH_NSIS=mingw32

RUN dnf install -y --nogpgcheck \
        cmake \
        findutils \
        make \
        ninja-build \
        python \
        python2 \
        git \
        wget \
        ccache \
        \
        extra-cmake-modules \
        qt5-qtbase-devel \
        qt6-qtbase-devel \
        qt6-qttools-devel \
        qt6-qtdeclarative-devel \
        qt6-qtremoteobjects-devel \
        qt6-linguist \
        appstream \
        \
        ${WINARCH}-qt6-qtactiveqt \
        ${WINARCH}-qt6-qtbase \
        ${WINARCH}-qt6-qtcharts \
        ${WINARCH}-qt6-qtdeclarative \
        ${WINARCH}-qt6-qtimageformats \
        ${WINARCH}-qt6-qtmultimedia \
        ${WINARCH}-qt6-qtsensors \
        ${WINARCH}-qt6-qtserialport \
        ${WINARCH}-qt6-qtsvg \
        ${WINARCH}-qt6-qttools \
        ${WINARCH}-qt6-qttranslations \
        ${WINARCH}-qt6-qtwebsockets \
        ${WINARCH}-angleproject \
        ${WINARCH}-gcc-c++ \
        ${WINARCH_NSIS}-nsis \
        ${WINARCH_NSIS}-nsiswrapper

RUN cd / && \
        git clone https://github.com/qt/qtremoteobjects.git && \
        cd qtremoteobjects && \
        git checkout v$(/usr/*-mingw32/bin/qt6/qmake -query QT_VERSION) && \
        ${WINARCH}-cmake -S . -B build -GNinja -DCMAKE_BUILD_TYPE=Release && \
        cmake --build build && \
        cmake --install build && \
        cd .. && \
        rm -rf qtremoteobjects

RUN cd / && \
        git clone https://github.com/qt/qtnetworkauth.git && \
        cd qtnetworkauth && \
        git checkout v$(/usr/*-mingw32/bin/qt6/qmake -query QT_VERSION) && \
        ${WINARCH}-cmake -S . -B build -GNinja -DCMAKE_BUILD_TYPE=Release && \
        cmake --build build && \
        cmake --install build && \
        cd .. && \
        rm -rf qtnetworkauth
